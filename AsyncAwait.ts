import * as needle from 'needle/lib/needle';

function makeRequest(url: string): Promise <any> {
    return new Promise((resolve, reject): void  => {
        needle.get(url, (error: any, data: any) => {
           if (!error) {
               resolve(data);
           }
           else {
               reject(error);
           }
        });
    });
}

async function getData(): Promise<any> {
    let response: Promise <any>;

    try {
        response = await makeRequest('http://media.oadts.com/www/delivery/avz.php?zoneid=4469&mode=v3w&cb=' + Date.now());
        console.log(response);
    }
    catch(e) {
        console.log(e);
    }
}

getData();